// Name our cache
const CACHE_NAME = 'v1';

const urlsToCache = [
    "charts",
    "contact",
    "info",
    "account",
    "login",
    "register",
    "index.html",
    "main.css",
    "ServiceWorkerBuilder.js",
    "ServiceWorkerInfo.js",
    "bundle.js",
    "0.chunk.js",
    "main.chunk.js",
    "react_devtools_backend.js",
    "manifest.json",
    "favicon.ico",
];

// The first time the user starts up the PWA, 'install' is triggered.
self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open(CACHE_NAME)
              .then((cache) => {
                  Promise.all(
                      urlsToCache.map(url => cache.add(url))
                  )
              })
    );
});

// Delete old caches that are not our current one!
self.addEventListener("activate", event => {
    event.waitUntil(
        caches.keys().then(keyList =>
            Promise.all(keyList.map(key => {
                if (!urlsToCache.includes(key)) {
                    console.log('Deleting cache: ' + key)
                    return caches.delete(key);
                }
            }))
        )
    );
});

// When the webpage goes to fetch files, we intercept that request and serve up the matching files
// if we have them
self.addEventListener('fetch', function (event) {
    event.respondWith(caches.match(event.request).then(function (response) {
        // caches.match() always resolves
        // but in case of success response will have value
        if (response !== undefined) {
            return response;
        } else {
            return fetch(event.request).then(function (response) {
                // response may be used only once
                // we need to save clone to put one copy in cache
                // and serve second one
                let responseClone = response.clone();

                caches.open(CACHE_NAME).then(function (cache) {
                    cache.put(event.request, responseClone);
                });

                return response;
            }).catch(function () {
                return response;
            });
        }
    }));
});