interface IUser {
  id?: any;
  login?: string;
}

type UserState = {
  id?: any;
  login?: string;
};

type LoginAction = {
  type: string;
  user: IUser;
};

type LogoutAction = {
  type: string;
};

type DispatchType = (args: UesrAction) => UserAction;
