import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import {createBrowserHistory} from 'history';
import {Route, Router, Switch} from 'react-router-dom';
import {applyMiddleware, createStore, Store} from 'redux';
import thunk from 'redux-thunk';

import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from './reportWebVitals';

import CssBaseline from '@material-ui/core/CssBaseline';

import ChartsComponent from './pages/charts/ChartsComponent';
import ContactsComponent from './pages/contacts/ContactsComponent';
import InfoComponent from './pages/info/InfoComponent';
import UserRegisterComponent from './pages/user/UserRegisterComponent';
import UserLogInComponent from './pages/user/UserLogInComponent';
import BottomNav from './bottom-nav/BottomNav';
import UserComponent from './pages/user/UserComponent';

import rootReducer from './store/reducer';
import HomeComponent from "./pages/home/HomeComponent";
import {ServiceWorkerWrapper} from "./ServiceWorkerWrapper";

const history = createBrowserHistory();

const store: Store<any, any> & {
    dispatch: DispatchType;
} = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render(
    <React.StrictMode>
        <CssBaseline/>

        <ServiceWorkerWrapper/>

        <Provider store={store}>
            <Router history={history}>
                <Switch>
                    <Route path='/charts'>
                        <ChartsComponent/>
                    </Route>

                    <Route path='/contact'>
                        <ContactsComponent/>
                    </Route>

                    <Route path='/info'>
                        <InfoComponent/>
                    </Route>

                    <Route path='/account/register'>
                        <UserRegisterComponent/>
                    </Route>

                    <Route path='/account/login'>
                        <UserLogInComponent/>
                    </Route>

                    <Route path='/account'>
                        <UserComponent/>
                    </Route>

                    <Route path='/'>
                        <HomeComponent/>
                    </Route>
                </Switch>

                <BottomNav/>
            </Router>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();

// new ServiceWorkerBuilder().add(new ServiceWorkerInfo('./cache.js')).registerAll();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

require('./main.css');
