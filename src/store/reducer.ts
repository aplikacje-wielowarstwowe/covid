import { combineReducers } from 'redux';

import UserReducer from '../pages/user/reducers/User.reducer';

const rootReducer = combineReducers({ user: UserReducer });

export default rootReducer;
