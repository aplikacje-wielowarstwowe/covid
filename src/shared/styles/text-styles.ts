import {createStyles, makeStyles, Theme} from "@material-ui/core";

export const textStyles = makeStyles((theme: Theme) =>
    createStyles({
        primaryText: {
            color: '#e1e1e1',
            fontSize: 'x-large',
        },
        secondaryText: {
            color: '#929292',
            fontSize: 'x-small',
        },
        errorText: {
            color: 'darkred',
        }
    }),
);