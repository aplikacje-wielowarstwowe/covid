import {withStyles} from '@material-ui/core/styles';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';

export const Accordion = withStyles({
    root: {
        margin: '25px 0',
        border: 'none',
        boxShadow: 'none',
        borderRadius: '4px',

        backgroundColor: '#252323',
        '&expanded': {
            margin: '25px',
        },
    },
    expanded: {},
})(MuiAccordion);

export const AccordionSummary = withStyles({
    root: {
        flexBasis: '100%',
        flexShrink: 0,
        color: '#e1e1e1',
    },
})(MuiAccordionSummary);


export const AccordionDetails = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
        color: '#929292',
        margin: '0 25px',
    },
}))(MuiAccordionDetails);
