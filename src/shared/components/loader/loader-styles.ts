import {createStyles, makeStyles, Theme} from "@material-ui/core";

export const loaderStyles = makeStyles((theme: Theme) =>
    createStyles({
        loaderContainer: {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignContent: 'center',
            alignItems: 'center',
        },

        loader: {
            margin: 'auto 0',
        },

        text: {
            fontSize: 'large',
            color: '#e1e1e1',
            textAlign: 'center',
            width: '100%',
            marginTop: '30px',
        },
    }),
);