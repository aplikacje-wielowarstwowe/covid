import React from "react";
import {loaderStyles} from "./loader-styles";
import {CircularProgress} from "@material-ui/core";

export const LoaderComponent: React.FunctionComponent<{ isLoading: boolean, text?: string, classNameForChildren?: any, createParentContainer?: boolean }> = (props) => {
    const classes = loaderStyles();

    const circularProgress = (
        <div className={classes.loaderContainer}>
            <CircularProgress disableShrink className={classes.loader}/>
            {props.text ? <p className={classes.text}>{props.text}</p> : <></>}
        </div>
    );

    const childrenInsideParentDiv = (
        props.createParentContainer
            ? <div className={props.classNameForChildren}>
                {props.children}
            </div>
            : <>
                {props.children}
            </>
    );

    return (
        props.isLoading
            ? circularProgress
            : childrenInsideParentDiv
    );
}