export default class ErrorDictionary {
    static get(code: string): string {
        switch (code) {

            case 'USER_NOT_FOUND':
                return 'Błędne dane logowania. Podaj poprawny login i/lub hasło.';

            case 'WRONG_PASSWORD':
                return 'Błędne dane logowania. Podaj poprawny login i/lub hasło.';

            case 'UNSAFE_PASSWORD':
                return 'Puste lub zbyt słabe hasło. Wymagane jest podanie hasła, które zawiera przynajmniej 8 znaków, ' +
                    'jedną małą i duzą literę oraz musi zawierać jedną cyfrę.';

            case 'USER_WITH_THAT_NAME_EXISTS':
                return 'Nazwa użytkownika jest już zajęta, podaj inną nazwę.';

            default:
                return '';
        }
    }
}