import {createStyles, makeStyles, Theme} from "@material-ui/core";

export const errorDialogStyles = makeStyles((theme: Theme) =>
    createStyles({
            root: {
                backgroundColor: '#302E2E',
                color: '#e1e1e1',
            },

            paper: {
                backgroundColor: '#302E2E',
                color: '#e1e1e1',
            },

            message: {
                color: '#e1e1e1',
            },
        },
    ),
);
