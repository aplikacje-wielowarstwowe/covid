import React from "react";
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@material-ui/core";
import {errorDialogStyles} from "./ErrorDialogStyles";

interface ErrorDialogProps {
    title?: string;
    message?: string;
    isOpen: boolean;
    onClose: any;
}

export const ErrorDialog: React.FunctionComponent<ErrorDialogProps> = (props) => {
    const styles: any = errorDialogStyles();

    return (
        <Dialog open={props.isOpen}
                onClose={props.onClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                classes={styles}>

            <DialogTitle id="alert-dialog-title">{props.title}</DialogTitle>

            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    <DialogContentText id="alert-dialog-description" className={styles.message}>
                        {props.message}
                    </DialogContentText>
                </DialogContentText>
            </DialogContent>

            <DialogActions>
                <Button onClick={props.onClose}
                        variant='contained'
                        color='primary'>
                    OK
                </Button>
            </DialogActions>
        </Dialog>
    );
}