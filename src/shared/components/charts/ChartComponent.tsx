import React, {useEffect, useState} from "react";
import {textStyles} from "../../styles/text-styles";
import {LoaderComponent} from "../loader/LoaderComponent";
import {LineChart} from "./line-chart/LineChart";
import ChartService from "./services/ChartService";
import ChartFilter from "./models/ChartFilter";
import ChartData from "./models/ChartData";

export const ChartComponent = (props: ChartFilter) => {
    let [chartData, setChartData] = useState(new ChartData());
    let [isFinish, setIsFinish] = useState(false);

    const classes: any = textStyles();

    useEffect(() => {
        if (!isFinish) {
            ChartService.getChartData({...props}, setChartData);
            setIsFinish(true);
        }
    }, [props, chartData, isFinish]);

    return (
        <LoaderComponent isLoading={!!chartData?.isLoading}
                         text={'Pobieranie statystyk'}
                         classNameForChildren={classes.container}>
            {!!chartData?.success
                ? <LineChart labels={chartData?.value?.labels} datasets={chartData?.value?.datasets}/>
                : <p className={classes.errorText}>{chartData?.error}</p>}
        </LoaderComponent>
    );
}