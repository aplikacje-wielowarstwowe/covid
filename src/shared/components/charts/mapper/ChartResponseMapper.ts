import ChartProps from "../models/ChartProps";
import DataSet from "../models/DataSet";

export default class ChartResponseMapper {
    static mapPerVoivodeship(response: any): ChartProps {
        const statistics: any = response?.statistics;
        if (!statistics) {
            return new ChartProps();
        }

        const voivodships: string[] = Object.keys(statistics);
        const items = Object.values(statistics);

        if (!voivodships || !items) {
            return new ChartProps();
        }

        let dates = items.flatMap((elem: any) => elem?.map((item: any) => item?.date));
        dates = dates.filter((item, pos, self) => self.indexOf(item) === pos);
        dates = dates.sort((a: any, b: any) => a.localeCompare(b));

        let datasets: DataSet[] = [];
        voivodships.forEach((voivodship: string) => {
            if (voivodship === 'Cały kraj') {
                return;
            }

            const values: number[] = statistics[voivodship].sort((a: any, b: any) => a.date.localeCompare(b.date))
                                                           .map((item: any) => item.cases);

            const dataset: DataSet = new DataSet(voivodship);
            dataset.data = values;
            datasets = [...datasets, dataset];
        });

        return new ChartProps(dates, datasets);
    }

    static mapPerCountry(result: any): ChartProps {
        const response: any[] = result?.response;
        if (!response) {
            return new ChartProps();
        }

        let dates: string[] = [];
        let datasets: DataSet[] = [new DataSet()];

        response.sort((a: any, b: any) => a.date.localeCompare(b.date))
                .forEach((item: any) => {
                    datasets[0].data = [...datasets[0].data, item.value];
                    dates = [...dates, item.date];
                });

        return new ChartProps(dates, datasets);
    }

    static mapDeathWithComorbiditiesPerCountry(result: any): ChartProps {
        const response: any[] = result?.response;
        if (!response) {
            return new ChartProps();
        }

        let dates: string[] = [];
        let datasets: DataSet[] = [
            new DataSet('przypadki śmiertlene'),
            new DataSet('przypadki śmiertlene wraz z chorobami współistniejącymi')
        ];

        response.sort((a: any, b: any) => a.date.localeCompare(b.date))
                .forEach((item: any) => {
                    datasets[0].data = [...datasets[0].data, item.value];
                    datasets[1].data = [...datasets[1].data, item.value_comorbidities];
                    dates = [...dates, item.date];
                });

        return new ChartProps(dates, datasets);
    }
}