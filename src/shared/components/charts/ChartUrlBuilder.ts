import ChartFilter from "./models/ChartFilter";
import {StatisticType} from "./models/StatisticType";
import {TimeIntervalType} from "./models/TimeIntervalType";

export default class ChartUrlBuilder {
    private static urls: string[] = [
        'https://sggwcovidapp.online:8877/api/statistics?startDate=$startDate&endDate=$endDate&timeIntervalType=$timeIntervalType&statisticType=$statisticType',
        'https://sggwcovidapp.online:5001/$statisticType?dateFrom=$dateFrom&dateUntil=$dateUntil&fbclid=IwAR3OxI456IIepr2c3FHUFG_3498L2_qY-zvZwM-HBvXW-dAUkStG1Tjozjs',
    ];

    static build(chartFilter: ChartFilter): string {
        switch (chartFilter.statisticType) {
            case StatisticType.DEATHS:
            case StatisticType.INFECTIONS:
                return this.urls[0].replace('$startDate', chartFilter.startDate || '2020-01-01')
                                   .replace('$endDate', chartFilter.endDate || new Date().toISOString().substring(0, 10))
                                   .replace('$timeIntervalType', chartFilter.timeIntervalType || TimeIntervalType.DAILY)
                                   .replace('$statisticType', chartFilter.statisticType);

            case StatisticType.TESTS:
            case StatisticType.CASES:
            case StatisticType.DEATHS_WITH_COMORBIDITIES:
                return this.urls[1].replace('$dateFrom', chartFilter.startDate || '2020-01-01')
                                   .replace('$dateUntil', chartFilter.endDate || new Date().toISOString().substring(0, 10))
                                   .replace('$statisticType', chartFilter.statisticType);

            default:
                throw new DOMException('Nie ma takiego typu statystyk (eid: 20200121002900)');
        }
    }
}