import ChartProps from "./ChartProps";

export default class ChartData {
    isLoading?: boolean;
    success?: boolean;
    error?: string;
    value?: ChartProps;

    constructor(isLoading: boolean = true, success: boolean = false, value?: ChartProps, error?: string) {
        this.isLoading = isLoading;
        this.success = success;
        this.value = value || new ChartProps();
        this.error = error || 'Nie można pobrać danych. Spróbuj odświeżyć stronę za kilka minut.';
    }

    areNotEquals(obj: ChartData): boolean {
        return this.value !== obj.value;
    }
}