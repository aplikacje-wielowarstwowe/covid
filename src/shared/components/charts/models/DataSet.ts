export default class DataSet {
    label: string;
    fill: boolean;
    lineTension: number;
    backgroundColor: string;
    borderColor: string;
    borderWidth: number;
    data: Array<number>;
    yAxisID: string;

    constructor(label?: string, fill?: boolean, lineTension?: number, backgroundColor?: string, borderColor?: string,
                borderWidth?: number, data?: Array<number>, yAxisID?: string) {
        this.label = label || '';
        this.fill = !!fill;
        this.lineTension = lineTension || 0.5;
        this.backgroundColor = backgroundColor || 'rgba(75,192,192,1)';
        this.borderColor = borderColor || `#${((1 << 24) * Math.random() | 0).toString(16)}`;
        this.borderWidth = borderWidth || 2;
        this.data = data || [];
        this.yAxisID = yAxisID || 'y-axis-1';
    }
}