import {TimeIntervalType} from "./TimeIntervalType";
import {StatisticType} from "./StatisticType";

export default class ChartFilter {
    startDate?: string;
    endDate?: string;
    timeIntervalType?: TimeIntervalType;
    statisticType: StatisticType;

    constructor(statisticType: StatisticType, startDate?: string, endDate?: string, timeIntervalType?: TimeIntervalType) {
        this.startDate = startDate || '2020-01-01';
        this.endDate = endDate || new Date().toISOString().substring(0, 10);
        this.timeIntervalType = timeIntervalType || TimeIntervalType.DAILY;
        this.statisticType = statisticType;
    }
}