export enum StatisticType {
    DEATHS = 'DEATHS',
    INFECTIONS = 'INFECTIONS',
    TESTS = 'tests',
    CASES = 'cases',
    DEATHS_WITH_COMORBIDITIES = 'deaths',
}