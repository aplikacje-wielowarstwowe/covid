import DataSet from "./DataSet";

export default class ChartProps {
    labels: Array<string> | undefined;
    datasets: Array<DataSet> | undefined;

    constructor(labels?: Array<string>, datasets?: Array<DataSet>) {
        this.labels = labels || [];
        this.datasets = datasets || [];
    }
}