import React from "react";
import ChartProps from "../models/ChartProps";
import {Line} from "react-chartjs-2";

export const LineChart: React.FunctionComponent<ChartProps> = (props) => {

    const options = {
        responsive: true,
        maintainAspectRatio: true,
        scales: {
            yAxes: [
                {
                    type: 'linear',
                    display: true,
                    position: 'left',
                    id: 'y-axis-1',
                },
                {
                    type: 'linear',
                    display: true,
                    position: 'right',
                    id: 'y-axis-2',
                    gridLines: {
                        drawOnArea: false,
                    },
                },
            ],
        },
    }

    return (
        <Line
            data={{...props}}
            options={options}
        />
    );
}