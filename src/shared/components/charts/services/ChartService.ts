import ChartProps from "../models/ChartProps";
import ChartResponseMapper from "../mapper/ChartResponseMapper";
import ChartUrlBuilder from "../ChartUrlBuilder";
import ChartFilter from "../models/ChartFilter";
import ChartData from "../models/ChartData";
import {Dispatch, SetStateAction} from "react";
import {StatisticType} from "../models/StatisticType";

export default class ChartService {
    private static caches: Map<string, ChartData> = new Map([]);

    static getChartData(chartFilter: ChartFilter, setChartData: Dispatch<SetStateAction<ChartData>>): void {
        let url = ChartUrlBuilder.build(chartFilter);

        if (this.doesDataExistInCacheByUrl(url)) {
            let dataFromCacheByUrl = this.getDataFromCacheByUrl(url);
            if (!!dataFromCacheByUrl) {
                setChartData(dataFromCacheByUrl);
                return;
            }
        }

        fetch(url)
            .then(response => response.json())
            .then((response) => {
                let mappedResponse: ChartProps;

                switch (chartFilter.statisticType) {

                    case StatisticType.DEATHS:
                    case StatisticType.INFECTIONS:
                        mappedResponse = ChartResponseMapper.mapPerVoivodeship(response);
                        break;

                    case StatisticType.TESTS:
                    case StatisticType.CASES:
                        mappedResponse = ChartResponseMapper.mapPerCountry(response);
                        break;

                    case StatisticType.DEATHS_WITH_COMORBIDITIES:
                        mappedResponse = ChartResponseMapper.mapDeathWithComorbiditiesPerCountry(response);
                        break;

                    default:
                        throw new DOMException('Nie ma takiego typu statystyk (eid: 20200121023300)');
                }

                const prevChartData: ChartData = new ChartData(false, true, mappedResponse);
                this.addDataToCache(url, prevChartData);
                setChartData(prevChartData);
            })
            .catch(reason => {
                console.log(reason);
                setChartData(new ChartData(false, false));
            });
    }

    private static addDataToCache(url: string, data: ChartData): void {
        this.caches.set(url, data);
    }

    private static getDataFromCacheByUrl(url: string): ChartData | undefined {
        return this.caches.get(url);
    }

    private static doesDataExistInCacheByUrl(url: string): boolean {
        return this.caches.has(url);
    }
}