import React, {CSSProperties, useEffect, useRef} from 'react';
import {Power3, TweenMax} from 'gsap';
import {sectionStyles} from './section-styles';

export interface SectionComponentProps {
  className?: string;
  children?: any;
  style?: CSSProperties;
}

export default function SectionComponent(props: SectionComponentProps) {
  let htmlElement: any = useRef(null);
  const styles = sectionStyles();

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    TweenMax.fromTo(
      htmlElement,
      1,
      { opacity: 0, y: 50, ease: Power3.easeOut },
      { opacity: 1, y: 0 }
    );
  }, []);

  return (
    <div
      className={`${styles.container} ${props.className}`}
      style={props.style}
      ref={(ref: any) => (htmlElement = ref)}
    >
      {props.children}
    </div>
  );
}
