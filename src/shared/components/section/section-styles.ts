import {createStyles, makeStyles, Theme} from "@material-ui/core";

export const sectionStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'grid',
            columnGap: '25px',
            rowGap: '35px',

            alignItems: 'center',
            justifyItems: 'center',

            margin: '25px',
            padding: '20px',
            backgroundColor: '#252323',
            borderRadius: '10px'
        },

        title: {
            fontSize: 'x-large',
            color: '#e1e1e1',
            textAlign: 'center',
        },

        description: {
            fontSize: 'small',
            color: '#929292',
        },

        table: {
            width: '100%',
            border: 'none',
            backgroundColor: 'transparent',
            borderRadius: '10px',
        },

        tableHeader: {
            border: 'none',
            backgroundColor: '#212121',
            color: '#e1e1e1 !important',
        },

        tableRow: {
            border: 'none',
            backgroundColor: '#424242',
            color: '#929292 !important',
        },
    }),
);