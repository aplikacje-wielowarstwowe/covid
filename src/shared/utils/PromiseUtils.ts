export default class PromiseUtils {
    static wait<T>(ms: number, value: T): Promise<T> {
        return new Promise<T>(resolve => setTimeout(() => resolve(value), ms));
    }
}