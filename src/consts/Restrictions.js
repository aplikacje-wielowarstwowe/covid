const RESTRICTIONS = [
  {
    title: 'OGRANICZENIA W PRZEMIESZCZANIU SIĘ',
    paragraphs: [
      '1,5 Metra – minimalna odległość między pieszymi',
      'Zasłanianie ust i nosa w miejscach publicznych',
    ],
  },
  {
    title: 'ŻYCIE SPOŁECZNE',
    paragraphs: [
      'Działalność placówek kultury w tym min. teatrów, kin, muzeów, galerii sztuk, domów kultury, jest zawieszona.',
      'Środkami transportu zbiorowego może podróżować ograniczona liczba osób.',
    ],
  },
  {
    title: 'UROCZYSTOŚCI RELIGIJNE',
    paragraphs: [
      'W kościołach obowiązuje limit osób - max. 1 osoba na 15 m2, przy przy zachowaniu odległości nie mniejszej niż 1,5 m. Jeżeli wydarzenie odbywa się na zewnątrz należy zachować dystans 1,5 m i zakryć usta i nos.',
      'Jeżeli wydarzenie odbywa się na zewnątrz należy zachować dystans 1,5 m i zakryć usta i nos.',
      'Ważne! W świątyniach i obiektach kultu religijnego obowiązkowe jest zakrywanie ust i nosa, z wyłączeniem osób sprawujących kult.',
    ],
  },
  {
    title: 'ZGROMADZENIA I SPOTKANIA',
    paragraphs: [
      'W zgromadzeniach publicznych może uczestniczyć max. 5 osób. Uczestnicy mają obowiązek zakrywania nosa i ust oraz zachowania 1,5 m odległości od innych osób. Ponadto odległość między zgromadzeniami nie może być mniejsza niż 100 m.',
      'W imprezach i spotkaniach organizowanych w domu może uczestniczyć maksymalnie 5 osób- do tego limitu nie wliczamy osoby zapraszającej na imprezę oraz osób, które wspólnie mieszkają lub gospodarują z tą osobą.',
    ],
  },
  {
    title:
      'WESELA, KOMUNIE, KONSOLACJE I INNE UROCZYSTOŚCI ORAZ PRZYJĘCIA OKOLICZNOŚCIOWE',
    paragraphs: ['Obowiązuje zakaz organizacji tego typu wydarzeń.'],
  },
  {
    title: 'KLUBY NOCNE, DYSKOTEKI I INNE MIEJSCA UDOSTĘPNIONE DO TAŃCZENIA',
    paragraphs: [
      'Obowiązuje zakaz działalności takich miejsc.',
      'Wyjątkiem są: sportowe kluby taneczne.',
    ],
  },
  {
    title: 'TARGI, WYSTAWY, KONGRESY, KONFERENCJE',
    paragraphs: [
      'Targi, wystawy, kongresy i konferencje mogą odbywać się wyłącznie on-line.',
    ],
  },
  {
    title: 'EDUKACJA',
    paragraphs: [
      'Uczniowie klas I-III szkół podstawowych od 18 stycznia uczą się stacjonarnie. Uczniowie klas szkół ponadpodstawowych, słuchacze placówek kształcenia ustawicznego oraz centrów kształcenia zawodowego, uczą się zdalnie.',
      'Ważne! Przedszkola i żłobki funkcjonują bez zmian.',
    ],
  },
  {
    title: 'ŻŁOBKI I PRZEDSZKOLA',
    paragraphs: [
      'Dzieci pracujących rodziców mogą uczęszczać do żłobków lub przedszkoli. Organy prowadzące te placówki mogą je prowadzić, mając na uwadze wytyczne Głównego Inspektoratu Sanitarnego, a także Ministerstwa Edukacji Narodowej oraz Ministerstwa Rodziny, Pracy i Polityki Społecznej.',
      'Ważne! Ze względu na sytuację epidemiologiczną organ prowadzący może ograniczyć liczebność grupy przedszkolnej lub ograniczyć liczbę dzieci objętych opieką w żłobkach.',
      'Ważne! Jednostka samorządu terytorialnego na podstawie dotychczasowych przepisów ogólnych może zamknąć wszystkie żłobki i przedszkola na swoim terenie.',
    ],
  },
  {
    title: 'GRANICE POLSKI',
    paragraphs: [
      'W Polska obowiązuje ruch graniczny w ramach granic wewnętrznych Unii Europejskiej. Oznacza to, że można podróżować i przekraczać granice wewnętrzne UE. Podróżni mają prawo do swobodnego wjazdu, wyjazdu oraz tranzytu przez terytorium RP. Nie muszą odbywać kwarantanny. Planując podróż zagraniczną, należy uwzględnić obostrzenia obowiązujące w kraju sąsiednim.',
    ],
  },
  {
    title: 'MIĘDZYNARODOWY RUCH KOLEJOWY',
    paragraphs: [
      'Obecnie funkcjonuje międzynarodowy ruch kolejowy w ramach granic wewnętrznych Unii Europejskiej.',
      'Ograniczenie dotyczy podróży pociągami relacji międzynarodowych poza granice zewnętrzne UE.',
    ],
  },
  {
    title: 'CENTRA HANDLOWE I SKLEPY WIELKOPOWIERZCHNIOWE',
    paragraphs: [
      'W placówkach handlowych, na targu lub poczcie, obowiązuje limit klientów:	1 os/10 m2 – w sklepach do 100 m2 oraz 1 os/15 m2  – w sklepach powyżej 100 m2',
    ],
  },
  {
    title: 'GODZINY DLA SENIORÓW',
    paragraphs: [
      'Od poniedziałku do piątku w godzinach 10:00 – 12:00 w sklepie, drogerii, aptece oraz na poczcie mogą przebywać wyłącznie osoby powyżej 60. roku życia.',
      'Wyjątkiem jest sytuacja, gdy wydanie produktu leczniczego, wyrobu medycznego albo środka spożywczego specjalnego przeznaczenia żywieniowego następuje w sytuacji zagrożenia życia lub zdrowia.',
    ],
  },
  {
    title: 'GASTRONOMIA',
    paragraphs: [
      'Obowiązuje zakaz działalności stacjonarnej restauracji. Możliwa jest jedynie realizacja usług polegających na przygotowywaniu i podawaniu żywności na wynos oraz w dowozie.',
      'Dozwolone jest prowadzenie restauracji w hotelach, ale tylko w dostarczaniu posiłków dla gości hotelowych do pokoi.',
    ],
  },
  {
    title: 'SALONY FRYZJERSKIE, KOSMETYCZNE, TATUAŻU',
    paragraphs: [
      'Stanowiska obsługi muszą znajdować się w odległości co najmniej 1,5 m od siebie, chyba że między tymi stanowiskami znajduje się przegroda o wysokości co najmniej 2 m od powierzchni podłogi.',
    ],
  },
];

export default RESTRICTIONS;
