export default class ServiceWorkerBuilder {
    add(serviceWorkerInfo) {
        if (!this.serviceWorkers) {
            this.serviceWorkers = [];
        }
        if (serviceWorkerInfo) {
            this.serviceWorkers = [...this.serviceWorkers, serviceWorkerInfo];
        }
        return this;
    }

    registerAll() {
        if ('serviceWorker' in navigator) {
            if (this.serviceWorkers) {
                this.serviceWorkers.forEach(value => this._loadServiceWorker(value));
            }
        } else {
            console.log('service worker is not supported');
        }
    }f

    _loadServiceWorker(serviceWorker) {
        window.addEventListener('load', function () {
            navigator.serviceWorker
                     .register(serviceWorker.scriptUrl, {scope: '/'})
                     .then(serviceWorker.success, serviceWorker.failure)
                     .catch(function (err) {
                         console.log(err)
                     });
        })
    }
}