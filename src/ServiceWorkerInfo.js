export default class ServiceWorkerInfo {
    constructor(scriptUrl, success, failure) {
        this.scriptUrl = scriptUrl;

        this.success = success || function (registration) {
            console.log(`${scriptUrl} registration successful with scope: ${registration.scope}`);
        };

        this.failure = failure || function (err) {
            console.log(`${scriptUrl} registration failed: ${err}`);
        };
    }
}