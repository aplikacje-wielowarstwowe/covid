import React, {useEffect, useState} from 'react';
import SectionComponent from '../../shared/components/section/SectionComponent';
import {useHistory} from 'react-router-dom';

import {Button} from '@material-ui/core';
import {sectionStyles} from '../../shared/components/section/section-styles';
import {userFormStyles} from './style';
import RestAPI from "../../shared/utils/RestAPI";
import {ErrorDialog} from "../../shared/components/error/ErrorDialog";
import ErrorDictionary from "../../shared/components/error/ErrorDictionary";
import {logIn, logOut} from "./actions/User.actions";
import {Dispatch} from "redux";
import {useDispatch, useSelector} from "react-redux";

class RegisterResult {
    isSuccess: boolean;
    message: string;

    constructor(isSuccess: boolean = true, message: string = '') {
        this.isSuccess = isSuccess;
        this.message = message;
    }
}

const UserRegisterComponent = () => {

    const classes = {...userFormStyles(), ...sectionStyles()};
    const history = useHistory();

    if (localStorage.getItem('login')) {
        history.replace('/account');
    }

    const [open, setOpen] = React.useState(false);
    const handleClose = () => {
        setOpen(false);
    };

    //NOTE: Two hooks, useSelector to get redux-store state, useDispatch to dispatch actions
    const user = useSelector(({user}: any) => user);
    const dispatch: Dispatch<any> = useDispatch();

    useEffect(() => {
        let loginValue = localStorage.getItem('login');
        if (user?.login) {
            history.replace('/account');
        } else if (!!loginValue) {
            dispatch(logIn({login: loginValue}));
            history.push('/account');
        } else {
            dispatch(logOut());
        }
    }, [history, user?.login]);

    const [loginValue, setLoginValue] = useState('');
    const [passwordValue, setPasswordValue] = useState('');
    const [registerResult, setRegisterResult] = useState(new RegisterResult());
    const register = (): void => {
        RestAPI.post(`https://sggwcovidapp.online:8091/registration?userName=${loginValue}&password=${passwordValue}`)
            .then(response => response.json())
            .then(response => {
                if (response?.success) {
                    localStorage.removeItem('login');
                    localStorage.setItem('login', loginValue);
                    dispatch(logIn({login: loginValue}));
                    history.replace('/account');
                } else {
                    response.message = ErrorDictionary.get(response?.error);
                    setRegisterResult(new RegisterResult(false, response?.message || 'Błąd rejestracji'));
                    setOpen(true);
                    console.log(`Błąd rejestrowania`);
                    console.log(response);
                }
            })
            .catch(error => {
                console.log(error);
                setRegisterResult(
                    new RegisterResult(
                        false,
                        'Utworzenie konta nie jest możliwe. Spróbuj ponownie za jakiś czas.'
                    )
                );
                setOpen(true);
            });
    };

    return (
        <SectionComponent>
            {
                !registerResult?.isSuccess
                    ? <ErrorDialog title={'Błąd rejestracji'}
                                   message={registerResult?.message}
                                   isOpen={open}
                                   onClose={handleClose}/>
                    : <></>
            }

            <p className={classes.title}>Załóż konto</p>

            <p className={classes.description}>
                Wypełnij poniższy formularz i korzystaj w pełni z możliwości oferowanych przez
                platformę.
            </p>

            <form>
                <div className={classes.formContainer}>
                    <div>
                        <p className={classes.label}>Login</p>
                        <input
                            type='text'
                            name='login'
                            className={classes.formInput}
                            onKeyDown={(event => event.keyCode === 13 ? register() : undefined)}
                            onChange={(event) => setLoginValue(event.target.value)}
                        />
                    </div>

                    <div>
                        <p className={classes.label}>Hasło</p>
                        <input
                            type='password'
                            name='password'
                            className={classes.formInput}
                            onKeyDown={(event => event.keyCode === 13 ? register() : undefined)}
                            onChange={(event) => setPasswordValue(event.target.value)}
                        />
                    </div>
                </div>

                <div className={classes.buttonsContainer}>
                    <Button
                        variant='contained'
                        color='primary'
                        className={classes.button}
                        onClick={register}>
                        Załóż konto
                    </Button>

                    <Button
                        variant='outlined'
                        color='primary'
                        className={classes.button}
                        onClick={() => history.replace('/account')}>
                        Wróć
                    </Button>
                </div>
            </form>
        </SectionComponent>
    );
};

export default UserRegisterComponent;
