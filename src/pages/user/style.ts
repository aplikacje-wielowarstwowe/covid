import {createStyles, makeStyles, Theme} from "@material-ui/core";

export const userFormStyles = makeStyles((theme: Theme) =>
    createStyles({
        formContainer: {
            width: '100%',
            display: 'grid',
            gridColumn: '1fr',
            rowGap: '20px',
            alignItems: 'stretch',
            justifyItems: 'stretch',
        },

        label: {
            fontSize: 'small',
            color: '#929292',
            marginBottom: '5px',
        },

        formInput: {
            width: '100%',
            height: '30px',
            padding: '8px',
            backgroundColor: '#302E2E',
            color: '#e1e1e1',
            borderRadius: '4px',
        },

        buttonsContainer: {
            width: '100%',
            marginTop: '50px',
            display: 'grid',
            gridColumn: '1fr',
            rowGap: '25px',
            alignItems: 'stretch',
            justifyItems: 'stretch',
        },

        button: {
            height: '50px',
        }
    })
);