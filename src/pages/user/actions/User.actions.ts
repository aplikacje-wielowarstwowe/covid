import * as actionTypes from '../../../store/actionTypes';

export const logIn = (user: IUser) => {
  const action: LoginAction = {
    type: actionTypes.LOG_IN,
    user,
  };

  return action;
};

export const logOut = () => {
  const action: LogoutAction = {
    type: actionTypes.LOG_OUT,
  };
  return action;
};
