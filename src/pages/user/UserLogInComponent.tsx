import React, {useEffect, useState} from 'react';
import {Button} from '@material-ui/core';
import {Dispatch} from 'redux';
import {useHistory} from 'react-router-dom';
import {sectionStyles} from '../../shared/components/section/section-styles';
import {userFormStyles} from './style';
import {useDispatch, useSelector} from 'react-redux';
import SectionComponent from '../../shared/components/section/SectionComponent';
import RestAPI from '../../shared/utils/RestAPI';
import {logIn, logOut} from "./actions/User.actions";
import {ErrorDialog} from "../../shared/components/error/ErrorDialog";
import ErrorDictionary from "../../shared/components/error/ErrorDictionary";

class LogInResult {
    isSuccess: boolean;
    message: string;

    constructor(isSuccess: boolean = true, message: string = '') {
        this.isSuccess = isSuccess;
        this.message = message;
    }
}

const UserLogInComponent = () => {
    const styles = {...userFormStyles(), ...sectionStyles()};
    const history = useHistory();

    const [open, setOpen] = React.useState(false);
    const handleClose = () => {
        setOpen(false);
    };

    const [loginValue, setLoginValue] = useState('');
    const [passwordValue, setPasswordValue] = useState('');
    const [logInResult, setLogInResult] = useState(new LogInResult());

    //NOTE: Two hooks, useSelector to get redux-store state, useDispatch to dispatch actions
    const user = useSelector(({user}: any) => user);
    const dispatch: Dispatch<any> = useDispatch();

    useEffect(() => {
        let loginValue = localStorage.getItem('login');
        if (user?.login) {
            history.replace('/account');
        } else if (!!loginValue) {
            dispatch(logIn({login: loginValue}));
            history.push('/account');
        } else {
            dispatch(logOut());
        }
    }, [history, user?.login]);

    const login = (): void => {
        RestAPI.post(`https://sggwcovidapp.online:8091/login?userName=${loginValue}&password=${passwordValue}`)
               .then(response => response.json())
               .then(
                   (response: any) => {
                       if (response?.success) {
                           localStorage.removeItem('login');
                           localStorage.setItem('login', loginValue);
                           dispatch(logIn({login: loginValue}));
                           history.push('/account');
                       } else {
                           response.message = ErrorDictionary.get(response?.error);
                           setLogInResult(new LogInResult(false, response?.message || 'Błąd logowania'));
                           setOpen(true);
                           console.log(`Błąd logowania`);
                           console.log(response);
                       }
                   }
               )
               .catch(error => {
                   console.log(error);
                   setLogInResult(
                       new LogInResult(
                           false,
                           'Logowanie nie jest możliwe. Spróbuj ponownie za jakiś czas.'
                       )
                   );
                   setOpen(true);
               });
    };

    return (
        <>
            <SectionComponent>
                {
                    !logInResult?.isSuccess
                        ? <ErrorDialog title={'Błąd logowania'}
                                       message={logInResult?.message}
                                       isOpen={open}
                                       onClose={handleClose}/>
                        : <></>
                }

                <p className={styles.title}>Zaloguj się</p>

                <p className={styles.description}>
                    Zaloguj się aby przejrzeć wszystie wyniki badań zapisane w historii.
                </p>

                <form className={styles.formContainer}>
                    <div>
                        <p className={styles.label}>Login</p>
                        <input
                            type='text'
                            name='login'
                            value={loginValue}
                            className={styles.formInput}
                            onKeyDown={(event => event.keyCode === 13 ? login() : undefined)}
                            onChange={(event) => setLoginValue(event.target.value)}
                        />
                    </div>

                    <div>
                        <p className={styles.label}>Hasło</p>
                        <input
                            type='password'
                            name='password'
                            value={passwordValue}
                            className={styles.formInput}
                            onKeyDown={(event => event.keyCode === 13 ? login() : undefined)}
                            onChange={(event) => setPasswordValue(event.target.value)}
                        />
                    </div>

                    <Button
                        variant='contained'
                        color='primary'
                        className={styles.button}
                        onClick={login}>
                        Zaloguj się
                    </Button>
                </form>
            </SectionComponent>

            <SectionComponent>
                <p className={styles.title}>Nie masz jeszcze konta?</p>

                <p className={styles.description}>
                    Nic nie szkodzi, kliknij poniżej przycisk "Załóż konto", wypełnij formularz i
                    korzystaj w pełni z możliwości oferowanych przez platformę.
                </p>

                <Button
                    variant='outlined'
                    color='primary'
                    className={styles.button}
                    onClick={() => history.push('/account/register')}>
                    Załóż konto
                </Button>
            </SectionComponent>
        </>
    );
};

export default UserLogInComponent;