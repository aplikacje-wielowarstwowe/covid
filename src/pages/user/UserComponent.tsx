import React, {useEffect} from 'react';
import {Button} from '@material-ui/core';
import {Dispatch} from 'redux';
import {useHistory} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import SectionComponent from '../../shared/components/section/SectionComponent';

import {logIn, logOut} from './actions/User.actions';
import {sectionStyles} from '../../shared/components/section/section-styles';
import {userFormStyles} from './style';

export default function UserComponent() {
    const styles = {...userFormStyles(), ...sectionStyles()};
    const history = useHistory();
    const user = useSelector(({user}: any) => user);
    const dispatch: Dispatch<any> = useDispatch();

    useEffect(() => {
        let loginValue = localStorage.getItem('login');
        if (!user?.login && !!loginValue) {
            dispatch(logIn({login: loginValue}));
        } else if (!user?.login && !loginValue) {
            dispatch(logOut());
            history.replace('/account/login');
        }
    }, [history, user?.login, dispatch]);

    const logout = () => {
        localStorage.removeItem('login');
        dispatch(logOut());
        history.replace('/account/login');
    };

    return (
        <React.Fragment>
            <SectionComponent>
                <p className={styles.title}>Witaj {user?.login}</p>

                <Button
                    variant='outlined'
                    color='primary'
                    className={styles.button}
                    onClick={logout}
                >
                    Wyloguj się
                </Button>
            </SectionComponent>
        </React.Fragment>
    );
}
