import * as actionTypes from '../../../store/actionTypes';

const initialState: UserState = {
  id: null,
  login: '',
};

const UserReducer = (state: UserState = initialState, action: any): UserState => {
  switch (action.type) {
    case actionTypes.LOG_IN:
      return {
        ...state,
        id: action.user.id,
        login: action.user.login,
      };
    case actionTypes.LOG_OUT:
      return {
        ...state,
        id: null,
        login: '',
      };
  }
  return state;
};

export default UserReducer;
