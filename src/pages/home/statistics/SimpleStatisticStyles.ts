import {createStyles, makeStyles, Theme} from "@material-ui/core";

export const simpleStatisticStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            rowGap: '10px',
        },

        primaryText: {
            color: '#e1e1e1',
            fontSize: 'xx-large',
            textAlign: 'center',
        },

        secondaryText: {
            color: '#929292',
            fontSize: 'medium',
            textAlign: 'center',
        },
    }),
);