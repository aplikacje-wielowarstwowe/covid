import React from "react";
import {textStyles} from "../../../shared/styles/text-styles";
import {simpleStatisticStyles} from "./SimpleStatisticStyles";
import SectionComponent from "../../../shared/components/section/SectionComponent";
import {LoaderComponent} from "../../../shared/components/loader/LoaderComponent";

class SimpleStatisticProps {
    isLoading?: boolean;
    success?: boolean;
    title: string;
    number?: number;
    error?: string;

    constructor(isLoading: boolean = false, success: boolean = true, title: string, number: number, error: string) {
        this.isLoading = isLoading;
        this.success = success;
        this.title = title;
        this.number = number;
        this.error = error;
    }
}

export const SimpleStatisticComponent: React.FunctionComponent<SimpleStatisticProps> = (props) => {
    const styles: any = {...textStyles(), ...simpleStatisticStyles()};

    const separateThousands = (number: number) => {
        return (number || 0).toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, " ");
    }

    return (
        <SectionComponent className={styles.container}>
            <LoaderComponent isLoading={!!props?.isLoading}
                             text={props.title}
                             classNameForChildren={styles.container}>
                {
                    !!props?.success
                        ? <>
                            <p className={styles.secondaryText}>{props.title}</p>
                            <p className={styles.primaryText}>{separateThousands(props?.number || 0)}</p>
                        </>
                        : <>
                            <p className={styles.secondaryText}>{props.title}</p>
                            <p className={styles.errorText}>{props?.error}</p>
                        </>
                }
            </LoaderComponent>
        </SectionComponent>
    );
}
