import React, {useEffect, useState} from "react";
import {SimpleStatisticComponent} from "./SimpleStatisticComponent";

class SimpleStatisticData {
    isLoading?: boolean;
    success?: boolean;
    cases?: number;
    deaths?: number;
    error?: string;

    constructor(isLoading: boolean = true, success: boolean = false, cases?: number, deaths?: number, error?: string) {
        this.isLoading = isLoading;
        this.success = success;
        this.cases = cases;
        this.deaths = deaths;
        this.error = error;
    }

    areNotEquals(obj: SimpleStatisticData): boolean {
        return this.cases !== obj.cases && this.deaths !== obj.deaths;
    }
}

let prevSimpleStatisticData: SimpleStatisticData = new SimpleStatisticData();

export default function ActualStatisticAboutTestResultComponent() {
    let [simpleStatisticData, setSimpleStatisticData] = useState(prevSimpleStatisticData);

    useEffect(() => {
        fetch('https://sggwcovidapp.online:5001/overall')
            .then(response => response.json())
            .then((response: SimpleStatisticData) => {
                if (prevSimpleStatisticData.areNotEquals(response)) {
                    prevSimpleStatisticData = new SimpleStatisticData(false, true,
                        response?.cases || 0, response?.deaths || 0,
                        "Nie można pobrać danych. Spróbuj odświeżyć stronę za kilka minut.");
                    setSimpleStatisticData(prevSimpleStatisticData);
                }
            })
            .catch(reason => {
                console.log(reason);
                if (!!prevSimpleStatisticData && prevSimpleStatisticData.areNotEquals(simpleStatisticData)) {
                    setSimpleStatisticData(prevSimpleStatisticData);
                }
            });
    }, [simpleStatisticData]);

    return (
        <>
            <SimpleStatisticComponent title={'ILOŚĆ ZGONÓW'}
                                      number={simpleStatisticData?.deaths}
                                      isLoading={simpleStatisticData?.isLoading}
                                      success={simpleStatisticData?.success}
                                      error={simpleStatisticData?.error}/>

            <SimpleStatisticComponent title={'ILOŚĆ POZYTYWNYCH PRZYPADKÓW'}
                                      number={simpleStatisticData?.cases}
                                      isLoading={simpleStatisticData?.isLoading}
                                      success={simpleStatisticData?.success}
                                      error={simpleStatisticData?.error}/>
        </>
    );
}