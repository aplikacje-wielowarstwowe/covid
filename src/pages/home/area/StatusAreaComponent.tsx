import React, {MutableRefObject, useEffect, useRef} from "react";
import {Power3, TweenMax} from "gsap";
import {statusAreaRedStyles, statusAreaStyles} from "./styles";
import {Areas} from "./StatusData";

export const StatusAreaComponent = (props: { area: Areas }) => {

    let htmlElement: MutableRefObject<any> = useRef(null);
    const classes = {...statusAreaStyles(), ...statusAreaRedStyles()};

    useEffect(() => {
        animateStateArea(htmlElement);
    }, [htmlElement]);

    return (
        <div className={`${classes.outerStatus} ${classes.statusArea}`}>
            <div className={`${classes.outerStatus} ${classes.internalStatus} ${classes.statusArea}`}
                 ref={(ref: any) => htmlElement = ref}>
            </div>
        </div>
    );
}

function animateStateArea(htmlElement: React.MutableRefObject<any>) {
    if (htmlElement) {
        TweenMax.fromTo(htmlElement, 1,
            {opacity: 1, scale: 1, ease: Power3.easeOut, repeat: -1, repeatDelay: 1},
            {opacity: 0, scale: 2, repeat: -1, repeatDelay: 1});
    }
}