import StatusData, {Areas} from "./StatusData";
import React from "react";
import {statusDataStyles} from "./styles";
import {textStyles} from "../../../shared/styles/text-styles";

export const StatusDataComponent = (props: { statusData: StatusData }) => {
    const classes = {...statusDataStyles(), ...textStyles()};

    return (
        <div className={classes.container}>
            <div>
                <p className={classes.secondaryText}>MIASTO</p>
                <p className={classes.primaryText}>{showCityNameOrEmptyValue(props.statusData.city)}</p>
            </div>

            <div>
                <p className={classes.secondaryText}>STREFA</p>
                <p className={classes.primaryText}>{convertColorName(props.statusData.area)}</p>
            </div>
        </div>
    );
}

function showCityNameOrEmptyValue(city: string | undefined) {
    return city ? city : 'Brak';
}

function convertColorName(area: Areas | undefined): string {
    switch (area) {
        case "green":
            return 'Zielona';

        case "yellow":
            return 'Żółta';

        case "red":
            return 'Czerwona';

        default:
            return 'Brak';
    }
}