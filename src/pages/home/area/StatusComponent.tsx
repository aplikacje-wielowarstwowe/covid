import React, { useEffect, useState } from 'react';
import SectionComponent from '../../../shared/components/section/SectionComponent';
import getUserCoords from './geolocation/GeolocationUtils';
import { LoaderComponent } from '../../../shared/components/loader/LoaderComponent';
import { StatusDataComponent } from './StatusDataComponent';
import StatusData from './StatusData';
import { StatusAreaComponent } from './StatusAreaComponent';
import { statusStyles } from './styles';
import { textStyles } from '../../../shared/styles/text-styles';
import RestrictionsInfoComponent from './RestrictionsInfoComponent';

let prevStatusDataRef: StatusData = new StatusData();

export default function StatusComponent() {
  let [statusData, setStatusData] = useState(prevStatusDataRef);
  const classes: any = { ...statusStyles(), ...textStyles() };

  useEffect(() => {
    if (!prevStatusDataRef?.city || !prevStatusDataRef?.area) {
      getUserCoords(onStatusDataChanged(statusData, setStatusData));
    }
  }, [statusData]);

  return (
    <SectionComponent>
      <LoaderComponent
        isLoading={!!statusData?.isLoading}
        text={'Pobieranie danych dot. strefy'}
        classNameForChildren={classes.container}
        createParentContainer={true}
      >
        {!!statusData?.success ? (
          <>
            <StatusAreaComponent area={statusData?.area} />

            <StatusDataComponent statusData={statusData} />

            <RestrictionsInfoComponent />
          </>
        ) : (
          <p className={classes.errorText}>{statusData?.error}</p>
        )}
      </LoaderComponent>
    </SectionComponent>
  );
}

function onStatusDataChanged(
  statusData: StatusData,
  setStatusData: (value: ((prevState: StatusData) => StatusData) | StatusData) => void
) {
  return (newValue: StatusData) => {
    if (
      !!newValue &&
      !!newValue.city &&
      (newValue.city !== statusData.city || newValue.area !== statusData.area)
    ) {
      prevStatusDataRef = newValue;
      setStatusData(newValue);
    }
  };
}
