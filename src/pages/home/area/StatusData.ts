export type Areas = 'green' | 'yellow' | 'red' | undefined;

export default class StatusData {
    city?: string;
    area?: Areas;
    isLoading?: boolean;
    error?: string;
    success?: boolean;

    constructor(city?: string, area?: Areas, isLoading: boolean = true, error?: string, success: boolean = false) {
        this.city = city;
        this.area = area;
        this.isLoading = isLoading;
        this.error = error;
        this.success = success;
    }
}