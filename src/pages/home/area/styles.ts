import {createStyles, makeStyles, Theme} from "@material-ui/core";

export const statusStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            width: '100%',
            display: 'grid',
            gridTemplateColumns: '100px 1fr',
            gridTemplateRows: '100px 1fr',
            justifyItems: 'stretch',
            alignItems: 'stretch',
        },
        accordionContainer: {
            border: 'none',
            boxShadow: 'none',
            marginTop: '35px',
            backgroundColor: '#302E2E',
            borderRadius: '4px',
            gridColumn: '1 / 3',
        },
        accordionHeading: {
            display: 'block',
            fontSize: theme.typography.pxToRem(15),
            flexBasis: '100%',
            flexShrink: 0,
            color: '#e1e1e1',
        },
        accordionAnswer: {
            color: '#929292',
        }
    }),
);

export const statusAreaStyles = makeStyles((theme: Theme) =>
    createStyles({
        outerStatus: {
            backgroundColor: 'gray',
            width: '50px',
            height: '50px',
            borderRadius: '50%',
            margin: '25px',
        },

        internalStatus: {
            margin: 0,
        }
    }),
);

export const statusAreaRedStyles = makeStyles((theme: Theme) =>
    createStyles({
        statusArea: {
            backgroundColor: 'red',
        },
    })
);

export const statusAreaYellowStyles = makeStyles((theme: Theme) =>
    createStyles({
        statusArea: {
            backgroundColor: 'yellow',
        },
    })
);

export const statusAreaGreenStyles = makeStyles((theme: Theme) =>
    createStyles({
        statusArea: {
            backgroundColor: 'green',
        },
    })
);

export const statusDataStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'grid',
            gridTemplateRows: '1fr 1fr',
            rowGap: '20px',
            justifyItems: 'center',
        },
    }),
);