export default interface GeolocationResponse {
    state: string,
    latt: string,
    longt: string,
    country: string,
    city: string,
    adminareas: any,
    staddress: string,
    stnumber?: any,
}
