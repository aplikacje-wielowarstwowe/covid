import GeolocationMapper from "./GeolocationMapper";
import GeolocationResponse from "./GeolocationResponse";
import PromiseUtils from "../../../../shared/utils/PromiseUtils";
import GeolocationData from "./GeolocationData";
import StatusData from "../StatusData";

export default function getUserCoords(onStatusDataChanged: (statusData: StatusData) => void): void {
    const geo: Geolocation = navigator.geolocation;

    if (geo) {
        geo.getCurrentPosition(
            position => successCallback(position, onStatusDataChanged),
            (reason: GeolocationPositionError) => errorCallback(reason, onStatusDataChanged),
            {enableHighAccuracy: true, maximumAge: 5000, timeout: 5000}
        );
    } else {
        console.log("Nie ma wsparcia dla Geolocation API bądź niezezwolono na dostęp do lokalizacji");
        onStatusDataChanged({
            isLoading: false,
            error: 'Nie ma wsparcia dla Geolocation API bądź niezezwolono na dostęp do lokalizacji'
        });
    }
}

const fetchGeolocationData = (latitude: number, longitude: number): Promise<GeolocationData | null> => {
    const url: string = `https://geocode.xyz/${latitude},${longitude}?geoit=json&region=PL&lang=PL`;
    return fetch(url).then((response: Response) => PromiseUtils.wait(5000, response))
                     .then((response: Response) => response.json())
                     .then((responseAsJson: GeolocationResponse) => GeolocationMapper.map(responseAsJson));
}

const successCallback = (location: GeolocationPosition, onStatusDataChanged: (statusData: StatusData) => void) => {

    // zapisanie szerokości i długości geograficznej do zmiennych
    const latitude = location.coords.latitude;
    const longitude = location.coords.longitude;

    fetchGeolocationData(latitude, longitude).then(value => {
        if (!!value && !!value.city) {
            onStatusDataChanged(new StatusData(value.city, 'red', false, '', true));
        } else {
            onStatusDataChanged({isLoading: false, success: true});
        }
    }).catch(
        reason => {
            console.log(reason);
            onStatusDataChanged({
                isLoading: false,
                success: false,
                error: 'Nie można pobrać informacji o lokalizacji. Spróbuj odświeżyć stronę za kilka minut.'
            });
        }
    );
};

const errorCallback = (positionError: GeolocationPositionError, onStatusDataChanged: (statusData: StatusData) => void) => {
    console.log(positionError);
    onStatusDataChanged({
        isLoading: false,
        success: false,
        error: 'Nie można pobrać informacji o lokalizacji. Spróbuj odświeżyć stronę za kilka minut.'
    });
}