import GeolocationResponse from "./GeolocationResponse";
import GeolocationData from "./GeolocationData";

export default class GeolocationMapper {
    static map(geolocationResponse: GeolocationResponse): GeolocationData {
        return {
            state: geolocationResponse.state,
            latt: geolocationResponse.latt,
            longt: geolocationResponse.longt,
            country: geolocationResponse.country,
            city: geolocationResponse.city,
            adminareas: GeolocationMapper.mapAdminarea(geolocationResponse),
            streetAddress: geolocationResponse.staddress,
        }
    }

    private static mapAdminarea(geolocationResponse: GeolocationResponse): string[] {
        if (!geolocationResponse?.adminareas) {
            return [];
        }

        const adminareas: any[] = Object.values(geolocationResponse.adminareas);
        if (!adminareas?.length) {
            return [];
        }

        const getOnlyNameProp: string[] = adminareas.map((adminarea: any) => adminarea?.name?.replace("ULICA", "").trim());
        const removeDuplication: Set<string> = new Set(getOnlyNameProp);
        return Array.from(removeDuplication);
    }
}