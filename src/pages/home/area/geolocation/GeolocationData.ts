export default interface GeolocationData {
    state: string;
    latt: string;
    longt: string;
    country: string;
    city: string;
    adminareas: string[];
    streetAddress: string;
    streetNumber?: string;
}