import React from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import { statusStyles } from './styles';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
} from '../../../shared/styles/accordion-styles';

import RESTRICTIONS from '../../../consts/Restrictions';

export default function RestrictionsInfoComponent() {
  const classes = statusStyles();
  const [expanded, setExpanded] = React.useState<string | false>(false);

  const handleChange = (panel: string) => (
    event: React.ChangeEvent<{}>,
    isExpanded: boolean
  ) => {
    setExpanded(isExpanded ? panel : false);
  };

  console.log(RESTRICTIONS);

  return (
    <Accordion
      className={classes.accordionContainer}
      key={0}
      expanded={expanded === `panel${0}`}
      onChange={handleChange(`panel${0}`)}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon style={{ color: '#e1e1e1' }} />}
        aria-controls={`panel${0}bh-content`}
        id={`panel${0}bh-content`}
      >
        <Typography className={classes.accordionHeading}>
          Jakie obowiązują obostrzenia w tej strefie?
        </Typography>
      </AccordionSummary>

      <AccordionDetails>
        <Typography className={classes.accordionAnswer}>
          <ol>
            {RESTRICTIONS.map(({ title, paragraphs }) => (
              <>
                <li style={{ margin: '10px 0' }}>{title}</li>
                <ul style={{ padding: '0 0 0 20px' }}>
                  {paragraphs.map((paragraph) => (
                    <li>{paragraph} </li>
                  ))}
                </ul>
              </>
            ))}
          </ol>
        </Typography>
      </AccordionDetails>
    </Accordion>
  );
}
