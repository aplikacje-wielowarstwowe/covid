import StatusComponent from './area/StatusComponent';
import ActualStatisticAboutTestResultComponent from './statistics/ActualStatisticAboutTestResultComponent';

export default function HomeComponent() {
  return (
    <>
      <StatusComponent />
      <ActualStatisticAboutTestResultComponent />
    </>
  );
}
