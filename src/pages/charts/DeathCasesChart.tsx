import React from "react";
import SectionComponent from "../../shared/components/section/SectionComponent";
import {ChartComponent} from "../../shared/components/charts/ChartComponent";
import {StatisticType} from "../../shared/components/charts/models/StatisticType";
import {sectionStyles} from "../../shared/components/section/section-styles";

export const DeathCasesChart = () => {
    const styles: any = sectionStyles();

    return (
        <SectionComponent>
            <p className={styles.title}>Statystyki dzień po dniu przypadków śmiertelnych z powodu koronawirusa (per województwo)</p>

            <ChartComponent statisticType={StatisticType.DEATHS}
                            startDate={'2020-12-25'}/>
        </SectionComponent>
    );
}