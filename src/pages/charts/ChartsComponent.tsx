import React, {useEffect, useState} from "react";
import {DeathCasesChart} from "./DeathCasesChart";
import SectionComponent from "../../shared/components/section/SectionComponent";
import {sectionStyles} from "../../shared/components/section/section-styles";
import {InfectionCasesChart} from "./InfectionCasesChart";
import {TestsPerformedChart} from "./TestsPerformedChart";
import {PositiveCasesChart} from "./PositiveCasesChart";
import {DeathWithComorbiditiesChart} from "./DeathWithComorbiditiesChart";

enum ScreenOrientation {
    PORTRAIT = 'PORTRAIT',
    LANDSCAPE = 'LANDSCAPE'
}

const getScreenOrientation = () => {
    return !window.screen.orientation.angle && !!navigator.maxTouchPoints
        ? ScreenOrientation.PORTRAIT
        : ScreenOrientation.LANDSCAPE;
};

export default function ChartsComponent() {
    const styles = sectionStyles();

    let [hasEventBeenAdded, setHasEventBeenAdded] = useState(false);
    let [canChartRender, setCanChartRender] = useState(getScreenOrientation() === ScreenOrientation.LANDSCAPE && window.screen.width >= 590);

    useEffect(() => {
        if (!hasEventBeenAdded) {
            window.addEventListener("resize", () => {
                setCanChartRender(getScreenOrientation() === ScreenOrientation.LANDSCAPE && window.screen.width >= 590);
            });
        }
        setHasEventBeenAdded(true);

        return () => window.removeEventListener("resize", () => {
        });
    }, [hasEventBeenAdded]);

    return (
        <>
            {
                canChartRender
                    ? <>
                        <DeathCasesChart/>

                        <InfectionCasesChart/>

                        <TestsPerformedChart/>

                        <PositiveCasesChart/>

                        <DeathWithComorbiditiesChart/>
                    </>
                    : <>
                        <SectionComponent>
                            <p className={styles.title}>Statystyki dot. koronawirusa</p>

                            <p className={styles.description}>
                                Aby zobaczyć wykresy obróć ekran (w pozycji poziomej).
                            </p>
                        </SectionComponent>
                    </>
            }
        </>
    );
}