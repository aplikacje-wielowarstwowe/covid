import React from "react";
import SectionComponent from "../../shared/components/section/SectionComponent";
import {ChartComponent} from "../../shared/components/charts/ChartComponent";
import {StatisticType} from "../../shared/components/charts/models/StatisticType";
import {sectionStyles} from "../../shared/components/section/section-styles";

export const PositiveCasesChart = () => {
    const styles: any = sectionStyles();

    return (
        <SectionComponent>
            <p className={styles.title}>Statystyki dzień po dniu pozytywnych wyników wykonanych testów na obecność
                koronawirusa</p>

            <ChartComponent statisticType={StatisticType.CASES}
                            startDate={'2020-12-25'}/>
        </SectionComponent>
    );
}