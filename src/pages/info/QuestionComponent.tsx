import React, {useEffect, useRef} from 'react';
import {Accordion, AccordionDetails, AccordionSummary,} from '../../shared/styles/accordion-styles';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {Typography} from '@material-ui/core';

const QuestionAccordion = ({questions}: { questions: any }) => {
    const [expanded, setExpanded] = React.useState<string>('');

    const handleChange = (panel: string) => (
        event: React.ChangeEvent<{}>,
        isExpanded: boolean
    ) => {
        setExpanded(isExpanded ? panel : '');
    };

    const itemsRef: any = useRef([]);

    useEffect(() => {
        itemsRef.current = itemsRef.current.slice(0, questions);
    }, [questions]);

    useEffect(() => {
        const idx: number = parseInt(expanded.replace('panel', ''));
        const elem: any = itemsRef.current[idx];
        window.scrollTo({top: elem?.offsetTop, behavior: 'smooth'});
    }, [expanded]);

    return (
        <React.Fragment>
            {questions.map((item: any, i: number) => (
                <Accordion
                    key={i} ref={el => itemsRef.current[i] = el}
                    expanded={expanded === `panel${i}`}
                    onChange={handleChange(`panel${i}`)}
                >
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon style={{color: '#e1e1e1'}}/>}
                        aria-controls={`panel${i}bh-content`}
                        id={`panel${i}bh-content`}
                    >
                        <Typography>{item.question}</Typography>
                    </AccordionSummary>

                    <AccordionDetails>
                        <Typography>{item.answer}</Typography>
                    </AccordionDetails>
                </Accordion>
            ))}
        </React.Fragment>
    );
};

export default QuestionAccordion;
