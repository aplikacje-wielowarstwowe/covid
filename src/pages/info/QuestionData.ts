export default class QuestionData {
    questions?: any[];
    isLoading?: boolean;
    error?: string;
    success?: boolean;

    constructor(questions: any[] = [], isLoading: boolean = true, error?: string, success: boolean = false) {
        this.questions = questions;
        this.isLoading = isLoading;
        this.error = error;
        this.success = success;
    }
}