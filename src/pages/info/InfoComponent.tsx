import React, {useEffect, useRef, useState} from 'react';
import {sectionStyles} from '../../shared/components/section/section-styles';
import QuestionAccordion from './QuestionComponent';
import QuestionData from "./QuestionData";
import {LoaderComponent} from "../../shared/components/loader/LoaderComponent";
import {textStyles} from "../../shared/styles/text-styles";
import {Power3, TweenMax} from "gsap";

const InfoComponent = () => {
    const [questions, setQuestions] = useState(new QuestionData());
    const classes = {...sectionStyles(), ...textStyles()};
    let htmlElement: any = useRef(null);

    useEffect(() => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
        TweenMax.fromTo(
            htmlElement,
            1,
            { opacity: 0, y: 50, ease: Power3.easeOut },
            { opacity: 1, y: 0 }
        );
    }, []);

    useEffect(() => {
        fetch('https://sggwcovidapp.online:4001/qa', {mode: 'cors'})
            .then((response) => response.json())
            .then((data: any[]) => {
                setQuestions(new QuestionData(data, false, '', true));
            })
            .catch(reason => {
                console.log(reason);
                setQuestions(new QuestionData([], false, 'Nie można było pobrać pytań. Spróbuj ponownie za jakiś czas.', false));
            });
    }, []);

    return (
        <div ref={(ref: any) => (htmlElement = ref)}>
            <p className={classes.title} style={{marginTop: '25px'}}>
                Pytania i odpowiedzi
            </p>

            <LoaderComponent
                isLoading={!!questions?.isLoading}
                text={'Pobieranie pytań'}
                classNameForChildren={classes.container}>
                {!!questions?.success ? (
                    <QuestionAccordion questions={questions?.questions}/>
                ) : (
                    <p className={classes.errorText}>{questions?.error}</p>
                )}
            </LoaderComponent>
        </div>
    );
};

export default InfoComponent;
