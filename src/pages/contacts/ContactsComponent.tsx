import React, {useEffect, useRef} from "react";
import {sectionStyles} from "../../shared/components/section/section-styles";
import {Power3, TweenMax} from "gsap";
import SectionComponent from "../../shared/components/section/SectionComponent";
import {AddressForEachVoivodeship} from "./AddressForEachVoivodeship";
import _voivodeships from './voivodeships.json';

export default function ContactsComponent() {
    const classes = sectionStyles();
    let htmlElement: any = useRef(null);

    useEffect(() => {
        window.scrollTo({top: 0, behavior: 'smooth'});
        TweenMax.fromTo(
            htmlElement,
            1,
            {opacity: 0, y: 50, ease: Power3.easeOut},
            {opacity: 1, y: 0}
        );
    }, []);

    const voivodeships = _voivodeships;

    return (
        <div ref={(ref: any) => (htmlElement = ref)}>
            <SectionComponent style={{marginTop: '25px', rowGap: '5px'}}>
                <p className={classes.title}>Centrum kontaktu COVID-19</p>

                <p className={classes.description} style={{textAlign: 'center'}}>
                    Infolinia czynna całodobowo
                    <br/>
                    +48 22 25 00 115
                </p>
            </SectionComponent>

            <AddressForEachVoivodeship voivodeships={voivodeships}/>
        </div>
    );
}