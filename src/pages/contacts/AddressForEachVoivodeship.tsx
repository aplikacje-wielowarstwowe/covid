import {Accordion, AccordionDetails, AccordionSummary} from "../../shared/styles/accordion-styles";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {Typography} from "@material-ui/core";
import {ChangeEvent, FunctionComponent, useEffect, useRef, useState} from "react";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Paper from "@material-ui/core/Paper";
import {sectionStyles} from "../../shared/components/section/section-styles";

export const AddressForEachVoivodeship: FunctionComponent<{ voivodeships: any }> = (props) => {
    const [expanded, setExpanded] = useState<string>('');
    const styles = sectionStyles();

    const handleChange = (panel: string) => (
        event: ChangeEvent<{}>,
        isExpanded: boolean
    ) => {
        setExpanded(isExpanded ? panel : '');
    };

    const itemsRef: any = useRef([]);

    useEffect(() => {
        itemsRef.current = itemsRef.current.slice(0, props.voivodeships.length);
    }, [props.voivodeships]);

    useEffect(() => {
        const idx: number = parseInt(expanded.replace('panel', ''));
        const elem: any = itemsRef.current[idx];
        window.scrollTo({top: elem?.offsetTop, behavior: 'smooth'});
    }, [expanded]);

    return (
        <div style={{margin: '25px'}}>
            {props.voivodeships.map((voivodeship: any, i: number) => (
                <Accordion
                    key={i} ref={el => itemsRef.current[i] = el}
                    expanded={expanded === `panel${i}`}
                    onChange={handleChange(`panel${i}`)}
                >
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon style={{color: '#e1e1e1'}}/>}
                        aria-controls={`panel${i}bh-content`}
                        id={`panel${i}bh-content`}
                    >
                        <Typography>{voivodeship?.name}</Typography>
                    </AccordionSummary>

                    <AccordionDetails>
                        <TableContainer component={Paper} className={styles.table}>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow className={styles.tableHeader}>
                                        <TableCell className={styles.tableHeader}>
                                            Nazwa placówki
                                        </TableCell>

                                        <TableCell align="center" className={styles.tableHeader}>
                                            Adres
                                        </TableCell>
                                    </TableRow>
                                </TableHead>

                                <TableBody>
                                    {voivodeship?.addresses?.map((address: any, idx: number) => (
                                        <TableRow key={address.name} className={styles.tableRow}>
                                            <TableCell component="th" scope="row"
                                                       className={styles.tableRow}
                                                       style={idx % 2 ? {backgroundColor: '#302E2E'} : {}}>
                                                {address.name}
                                            </TableCell>

                                            <TableCell align="center"
                                                       className={styles.tableRow}
                                                       style={idx % 2 ? {backgroundColor: '#302E2E'} : {}}>
                                                {address.address}
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </AccordionDetails>
                </Accordion>
            ))}
        </div>
    );
};
