import {createMuiTheme} from "@material-ui/core/styles";
import {createStyles, makeStyles, Theme} from "@material-ui/core";

export const bottomNavStyles = makeStyles((theme: Theme) =>
    createStyles({
        nav: {
            top: 'auto',
            bottom: 0,
            height: '83px',
            paddingTop: "10px",
            paddingBottom: "34px",
            width: "100%",
            position: "fixed",
        },
    }),
);

export const bottomNavTheme = createMuiTheme({
    palette: {
        background: {
            paper: '#302E2E',
        },
        primary: {
            main: '#E4E4E4',
        },
        text: {
            secondary: '#565656',
        },
    },
});