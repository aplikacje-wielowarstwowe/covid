import React from 'react';

import {ThemeProvider} from '@material-ui/core/styles';

import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';

import {useHistory} from 'react-router-dom';
import {bottomNavStyles, bottomNavTheme} from "./styles";
import getIcon from "./icons";

export default function BottomNav() {
    const [value, setValue] = React.useState(getCurrentUrl());
    const history = useHistory();
    const classes = bottomNavStyles();

    return (
        <ThemeProvider theme={bottomNavTheme}>
            <BottomNavigation value={value}
                              onChange={(event, newValue) => setValue(newValue)}
                              className={classes.nav}
                              showLabels>
                <BottomNavigationAction value="home" aria-label={"strona główna"}
                                        icon={getIcon('home', value)}
                                        onClick={() => history.replace("/")}/>

                <BottomNavigationAction value="charts" aria-label={"statystyki"}
                                        icon={getIcon('charts', value)}
                                        onClick={() => history.replace("/charts")}/>

                <BottomNavigationAction value="contact" aria-label={"Kontakt"}
                                        icon={getIcon('contact', value)}
                                        onClick={() => history.replace("/contact")}/>

                <BottomNavigationAction value="info" aria-label={"Informacje"}
                                        icon={getIcon('info', value)}
                                        onClick={() => history.replace("/info")}/>

                <BottomNavigationAction value="account" aria-label={"Konto użytkownika"}
                                        icon={getIcon('account', value)}
                                        onClick={() => history.replace("/account")}/>
            </BottomNavigation>
        </ThemeProvider>
    );
};

function getCurrentUrl(): string {
    return window.location.pathname.substr(1) || "home";
}
