import {Theme} from "@material-ui/core/styles";

export type Props = {
    classes: (theme?: Theme) => void;
}

export type Context = {
    value: string;
}