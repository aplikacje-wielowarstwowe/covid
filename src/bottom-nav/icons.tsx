import HomeIcon from "@material-ui/icons/Home";
import HomeOutlinedIcon from "@material-ui/icons/HomeOutlined";
import ShowChartIcon from "@material-ui/icons/ShowChart";
import ShowChartOutlinedIcon from "@material-ui/icons/ShowChartOutlined";
import PermPhoneMsgIcon from "@material-ui/icons/PermPhoneMsg";
import PermPhoneMsgOutlinedIcon from "@material-ui/icons/PermPhoneMsgOutlined";
import InfoIcon from "@material-ui/icons/Info";
import InfoOutlinedIcon from "@material-ui/icons/InfoOutlined";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import React from "react";

export default function getIcon(url: string, value: string) {
    switch (url) {
        case 'home':
            return value?.includes('home')
                ? <HomeIcon fontSize="large"/>
                : <HomeOutlinedIcon />;

        case 'charts':
            return value?.includes('charts')
                ? <ShowChartIcon fontSize="large"/>
                : <ShowChartOutlinedIcon/>

        case 'contact':
            return value?.includes('contact')
                ? <PermPhoneMsgIcon fontSize="large"/>
                : <PermPhoneMsgOutlinedIcon/>

        case 'info':
            return value?.includes('info')
                ? <InfoIcon fontSize="large"/>
                : <InfoOutlinedIcon/>
        
        case 'account':
            return value?.includes('account')
                ? <AccountCircleIcon fontSize="large"/>
                : <AccountCircleOutlinedIcon/>

        default:
            throw new DOMException("Nie istnieje taki url");
    }
}